%{
#include <stdio.h>


#define YY_DECL extern "C" int yylex()
#include "scanner.tab.h"
%}

ws [ \t]+
digit [0-9]
number {digit}+
id [a-zA-Z0-9]+

%%

{ws}       ;
\{ {return OPENBR;}
\} {return CLOSEBR;}
\: {return COL;}
{number} {return NUMBER;}
\" {return PAR;}
{id} {return ID;}
\[ {return SREDNJALIJEVA;}
\] {return SREDNJADESNA;}
, {return COMMA;}
.          ;

%%





















