%{
#include <cstdio>
#include <iostream>
using namespace std;

extern "C" int yylex();
extern "C" int yyparse();
extern "C" FILE *yyin;
extern "C" int line_num; 

void yyerror(const char *s);
%}

%token OPENBR
%token CLOSEBR
%token COL
%token COMMA
%token NUMBER
%token PAR
%token ID
%token SREDNJALIJEVA
%token SREDNJADESNA

%%

program:
        OPENBR content CLOSEBR
        ;
content: 
        something COL something content2
        ;
content2:
        COMMA content 
        | %empty
        ;
something: 
        PAR ID PAR 
        | NUMBER
        | srednjaizraz
        | content
        ;
srednjaizraz:
        SREDNJALIJEVA something COL something SREDNJADESNA 
        ;
        
%%

int main()
{
do {
yyparse();
} while (!feof(yyin));

cout <<"Input fajl validan"<<endl;

return 0;
}

void yyerror(const char *s)
{
cout<<"Input fajl nije validan, greska u liniji" <<endl;
exit(-1);
}


