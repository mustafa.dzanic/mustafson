zadatak3: lex.yy.c scanner.tab.c
	g++ -std=c++11 lex.yy.c scanner.tab.c -lfl -o zadatak3

lex.yy.c: scanner.l scanner.tab.h
	flex scanner.l

scanner.tab.c scanner.tab.h: scanner.y
	bison -d scanner.y

clean:
	rm -rf lex.yy.c scanner.tab.c scanner.tab.h zadatak3
